/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graph;

import java.util.HashMap;

/**
 *
 * @author Eric Montufar
 */
public class NodeGraph {
    
     public static NodeGraph newNode = null;
        int Id, grade =0,distance;
        double x, cluster;                                        // Node Properties
        double y;
        HashMap<Integer,NodeGraph>  adyNodes;
        HashMap<Integer,VerticeGraph>  Edges;
        boolean visited = false;

        private NodeGraph(){                            //Singleton
        }
        
                
        public static NodeGraph createNode(){
            
            if (newNode==null){
                newNode = new NodeGraph();  
            }
            return newNode;
        }
        
        
      
        public NodeGraph(int ID){                      //Constructor with ID

            this.Id=ID;
            adyNodes  = new HashMap<>(); 
            Edges = new HashMap<>();
            this.cluster=ID;
        }

        
//        public static NodeGraph createNode(int Id,int x, int y, int grade, HashMap adyNodes){     //Creates a node for Geographic method
//            
//            NodeGraph newNode= new NodeGraph(Id);                                                 //Creaates a Node
//            newNode.x = x;
//            newNode.y = y;                                                                        //Node X and Y coordinates
//            newNode.grade = 0;                                                                    //Grade of the node with 0 as value
//            newNode.adyNodes = adyNodes;                                                          //HashMap of Adyacent Nodes 
//            return newNode;
//        }
        
        
        public static NodeGraph createNode(int Id, int grade, HashMap adyNodes){    //Creates a node for any other method 
            
            
            NodeGraph newNode= new NodeGraph(Id);                                   //Creaates a Node
            newNode.grade = 0;                                                      //Grade of the node with 0 as value                                         //HashMap of Adyacent Nodes 
            newNode.adyNodes  = new HashMap<>(); 
            newNode.Edges = new HashMap<>();
            newNode.visited= false;
            newNode.distance= 0;
            newNode.cluster = (int) Math.random()*1000;
            
            
            return newNode;
            
      
        }
        
        
        
    
//    @Override
//    public String toString(){
//    
//        
//     String salida = 
//        
//   }
    
}
