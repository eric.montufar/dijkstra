/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graph;

import java.io.IOException;
import java.util.HashMap;
import java.util.Queue;

/**
 *
 * @author Eric Montufar
 */
public class Graph {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        
        RandomGraph g1 = new RandomGraph();
        RandomGraph g2 = new RandomGraph();
        RandomGraph g3 = new RandomGraph();
        TreeGraph g4 = new TreeGraph();
        
        //genErdosRenyi(number of nodes, number of vertices, directed graph, allows autocicles, filename) 
        
//          HashMap gr1 = g1.genErdosRenyi(20, 100, false, false, "ErdosRenyi_20");  
//          g4.IterDFS(gr1, false, "ErdosRenyi_20_IterativeDFS");
//          g4.CreateDFS(gr1, false, "ErdosRenyi_20_RecursiveDFS");
//          g4.createBFS(gr1, false, "ErdosRenyi_20_BFS");
//       

//
        HashMap gr2 = g1.genErdosRenyi(20, 100, false, false, "ErdosRenyi_100");
        g4.Kruskal(gr2, false, "Dijkstra_Erdos_20");
//        g4.CreateDFS(gr2, false, "ErdosRenyi_100_DFS");
//        g4.createBFS(gr2, false, "ErdosRenyi_100_BFS");  
//        
////        
//        HashMap gr3 = g1.genErdosRenyi(100, 2000, false, false,"ErdosRenyi_500");
//        g4.Prim(gr3, false, "Dijkstra_Erdos_100");
//        g4.CreateDFS(gr3, false, "ErdosRenyi_500_RecursiveDFS");
//        g4.IterDFS(gr3, false, "ErdosRenyi_500_IterativeDFS");
//         g4.createBFS(gr3, false, "ErdosRenyi_500_BFS");
//



        //genGilbert(number of nodes, probability of creating a node, directed graph, allows autocicles, filename) 
        
//       HashMap gr4 = g2.genGilbert(20, .3, false, false, "Glbert_20_directed");
//       g4.Prim(gr4, false, "Dijkstra_Gilbert_20");
//        g4.IterDFS(gr4, false, "Gilbert_20_IterativeDFS");
//        g4.CreateDFS(gr4, false, "Gilbert_20_DFS");
//        g4.createBFS(gr4, false, "Gilbert_20_BFS");
        
        
//        HashMap gr5 = g2.genGilbert(100, .3, false, false, "gilbert_100_directed");
//        g4.CreateDFS(gr5, false, "gilbert_100_DFS");
//        g4.createBFS(gr5, false, "gilbert_100_BFS");
//        
//        HashMap gr6 = g2.genGilbert(500, .3, false, false, "Gilbert_500_directed");
//        g4.Prim(gr6, false, "Dijkstra_Gilbert_500");
//        g4.IterDFS(gr6, false, "Gilbert_500_IterativeDFS");
//        g4.CreateDFS(gr6, false, "Gilbert_500_DFS");
//        g4.createBFS(gr6, false, "Gilbert_500_BFS");
////        
//
////        //public void GenBarabasiAlbert(number of nodes, max grade of the node, directed graph, allows autocicles, filename)
////      
//        HashMap gr7 = g3.GenBarabasiAlbert(20, 8, false, false, "Barabasi_20");
//        g4.Prim(gr7, false, "Dijkstra_Barabasi_20");
//        g4.IterDFS(gr7, false, "Barabasi_20_IterativeDFS");
//        g4.CreateDFS(gr7, false, "Barabasi_20_RecursiveDFS");
//        g4.createBFS(gr7, false, "Barabasi_20_BFS"); 
//        
//          HashMap gr8 = g3.GenBarabasiAlbert(100, 20, false, false, "Barabasi_100");
//          g4.CreateDFS(gr8, false, "Barabasi_100_DFS");
//          g4.createBFS(gr8, false, "Barabasi_100_BFS"); 
////
//        HashMap gr9 = g3.GenBarabasiAlbert(500, 100, false, false, "Barabasi_500");
//         g4.Prim(gr9, false, "Dijkstra_Barabasi_500");
//        g4.IterDFS(gr9, false, "Barabasi_500_IterativeDFS");
//        g4.CreateDFS(gr9, false, "Barabasi_500_RecursiveDFS");
//        g4.createBFS(gr9, false, "Barabasi_500_BFS");         
//// 
//        
////        //genGeographic(number of nodes, radius, directed graph, allows autocicles)
////        
//       HashMap gr10 = g3.genGeographic(20, .6, false, false, "Geographic_20");
//       g4.Prim(gr10, false, "Dijkstra_Geographic_20");
//        g4.IterDFS(gr10, false, "Geographic_20_IterativeDFS");
//        g4.CreateDFS(gr10, false, "Geographic_20_recursiveDFS");
//        g4.createBFS(gr10, false, "Geographic_20_BFS");   
//        
//        HashMap gr11 = g3.genGeographic(100, .25, false, false, "Geographic_100");
//        g4.CreateDFS(gr11, false, "Geographic_100_DFS");
//        g4.createBFS(gr11, false, "Geographic_100_BFS");   
////        
//       HashMap gr12 = g3.genGeographic(500, .35, false, false, "Geographic_500");
//       g4.Prim(gr12, false, "Dijkstra_Geographic_500");
//        g4.IterDFS(gr12, false, "Geographic_500_IterativeDFS");
//        g4.CreateDFS(gr12, false, "Geographic_500_RecursiveDFS");
//        g4.createBFS(gr12, false, "Geographic_500_BFS");   
//        

    }
    
}
