  /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graph;


import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;


public class TreeGraph {
  
    
    HashMap<String, VerticeGraph> TreeVertices; 
    HashMap<Integer,NodeGraph> TreeNodes ;
    
    HashMap<String,NodeGraph> adyNodes;
    HashMap<Integer,NodeGraph> GraphNodes;

    Queue <NodeGraph> treeQueue;
    
    
    
   
    public void createBFS (HashMap Nodes, boolean directed, String filename) throws IOException{

        TreeNodes = new HashMap<>();
        TreeVertices = new HashMap<>();

        for(int k=1; k<= Nodes.size(); k++){
            NodeGraph N = (NodeGraph) Nodes.get(k);
            N.visited=false;
        }
        Queue<NodeGraph> treeQueue = new LinkedList<>();
        int node1 = (int)Math.ceil(Math.random() * Nodes.size());
        NodeGraph N1 = (NodeGraph) Nodes.get(node1); 
        
        TreeNodes.put(1, N1);
        treeQueue.add(N1);
        N1.visited = true;
               
        while (!treeQueue.isEmpty()){
             Queue<NodeGraph> nextQueue = new LinkedList<>();
             for(int i=1; i<= treeQueue.size();i++){
              
                 NodeGraph Node1 = treeQueue.peek();
                 treeQueue.poll();
                 for (int j =1; j<= Node1.adyNodes.size();j++){
                     NodeGraph Node2 = Node1.adyNodes.get(j);
                     if(Node2.visited == false){
                         
                         Node2.visited = true;
                         nextQueue.add(Node2);
                         TreeNodes.put(TreeNodes.size()+1, Node2);
                         
                         String Idv = (Node1.Id+"--"+Node2.Id+";");                                      //Create a string for the Vertice Id
                         VerticeGraph newVertice = VerticeGraph.createVertice( Idv, Node1, Node2, false);        // Creates Vertice
                         TreeVertices.put(newVertice.Idv, newVertice);                                                      //put vertice into the HashMaps of Vertices
                         VerticeGraph.writeFile(newVertice.Idv, newVertice.weight ,filename);                                       //Add vertice to the .gv file
                     }
                 }
                 treeQueue = nextQueue;
                 
             }
            
        }
   
    }
    
    
   
    
    
    
    
    
    
        public void CreateDFS(HashMap Nodes, boolean directed, String filename) throws IOException{
            
            
            TreeNodes = new HashMap<>();
            TreeVertices = new HashMap<>();
        
            for(int k=1; k<= Nodes.size(); k++){
                NodeGraph N = (NodeGraph) Nodes.get(k);
            N.visited=false;
            }
            

            int node1 = (int)Math.ceil(Math.random() * Nodes.size());
            NodeGraph N1 = (NodeGraph) Nodes.get(node1); 
            TreeNodes.put(1,N1);
            
            Stack<NodeGraph> treeStack = new Stack<>();
            treeStack.push(N1);
            TreeGraph.DFS(treeStack, TreeVertices, TreeNodes, filename);
           
            
    }
        public static HashMap DFS(Stack treeStack, HashMap TreeVertices, HashMap TreeNodes,String filename) throws IOException{

            if(treeStack.isEmpty()){
                return TreeVertices;
                
            }
            while(!treeStack.isEmpty()){
                NodeGraph Node1 = (NodeGraph) treeStack.peek();
              //  if (Node1.visited==false){
                    Node1.visited = true;
                    for(int i = 1; i<=Node1.adyNodes.size(); i++){
                        NodeGraph Node2 = Node1.adyNodes.get(i);
                        if (Node2.visited==false){
                            
                            TreeNodes.put(TreeNodes.size()+1, Node2);
                            String Idv = (Node1.Id+"--"+Node2.Id+";");                                      //Create a string for the Vertice Id
                            VerticeGraph newVertice = VerticeGraph.createVertice( Idv, Node1, Node2, false);        // Creates Vertice
                            TreeVertices.put(newVertice.Idv, newVertice);                                                      //put vertice into the HashMaps of Vertices
                            VerticeGraph.writeFile(newVertice.Idv, newVertice.weight ,filename);                                       //Add vertice to the .gv file
                            treeStack.push(Node2);
                            return DFS(treeStack, TreeVertices, TreeNodes, filename); 
                        }
                        if (i==Node1.adyNodes.size()){
                          treeStack.pop();  
                          return DFS(treeStack, TreeVertices, TreeNodes, filename); 
                        }

                    }
    
                }
            //}
            return TreeVertices;
        }
    
    
             public void IterDFS(HashMap Nodes, boolean directed, String filename) throws IOException{

             Stack<NodeGraph> treeStack = new Stack<>();
             TreeNodes = new HashMap<>();
             TreeVertices = new HashMap<>();
            
            for(int k=1; k<= Nodes.size(); k++){
                NodeGraph N = (NodeGraph) Nodes.get(k);
                N.visited=false;
            }
            
            int node1 = (int)Math.ceil(Math.random() * Nodes.size());
            NodeGraph N1 = (NodeGraph) Nodes.get(node1); 
            N1.visited=true;
            TreeNodes.put(1,N1);
            treeStack.push(N1);
           
            
            
            while(!treeStack.isEmpty()){
                NodeGraph Node1= treeStack.peek();
                boolean flag1 = false;
                 for (int i=1;i<=Node1.adyNodes.size(); i++){
                     
                    NodeGraph Node2 = Node1.adyNodes.get(i);
                    //System.out.println(Node1.adyNodes.size()+"  " +i);
                    if (!Node2.visited && Node2!=null){
               
                        i=Node1.adyNodes.size();
                        treeStack.push(Node2);
                        Node2.visited= true;
                        String Idv = (Node1.Id+"--"+Node2.Id+";"); 
                        TreeNodes.put(TreeNodes.size()+1, Node2);                                   //Create a string for the Vertice Id
                        VerticeGraph newVertice = VerticeGraph.createVertice( Idv, Node1, Node2, false);        // Creates Vertice
                        TreeVertices.put(newVertice.Idv, newVertice);                                                      //put vertice into the HashMaps of Vertices
                        VerticeGraph.writeFile(newVertice.Idv , newVertice.weight,filename);                                       //Add vertice to the .gv file
                        flag1 = true;
                    }
                    if(flag1 ==  false && i==Node1.adyNodes.size()){
                        treeStack.pop();
                      
                    }
                }

             }
            
}
             
             
             
             
                     
    
            
            public void Dijkstra(HashMap Vertices, boolean directed, String filename){
                
                String EdgeKey;
                int weightSum=0;
                
        
                Queue<VerticeGraph> queue=new PriorityQueue<VerticeGraph>();
          
                Set<HashMap.Entry<String,VerticeGraph>> entrySet = Vertices.entrySet();
                for(Entry<String, VerticeGraph> e : entrySet) {
                    String key = e.getKey();
                    
                    
                    VerticeGraph V= (VerticeGraph) Vertices.get(key);
                    
                    NodeGraph Node1;
                    Node1 = V.Node1;
    
                    for (int i=1;i<Node1.adyNodes.size();i++){
                        NodeGraph Node2 = Node1.adyNodes.get(i);
                        System.out.println(Node2.Id);
  
                    }
//                    queue.add(V);
//                    System.out.println(V.Idv+"  "+V.weight);
                 }
                
                for(VerticeGraph b:queue){  
                    System.out.println("................................................................");
                    System.out.println(b.Idv+"  "+b.weight);  
                 }  

                
                
            }
            
            
         
            
            
        public void Prim(HashMap Vertices, boolean directed, String filename) throws IOException{
                
                String EdgeKey;
                int weightSum=0;
                     
                
                double n= Math.random();
                TreeNodes = new HashMap<>();
                TreeVertices = new HashMap<>();

                Queue<VerticeGraph> queue=new PriorityQueue<VerticeGraph>();
          
                Set<HashMap.Entry<String,VerticeGraph>> entrySet = Vertices.entrySet();
                int N1= (int) Math.ceil(Math.random()*Vertices.size()*(0.02));
                NodeGraph Node1 = null;
                for(Entry<String, VerticeGraph> e : entrySet) {
                    String key = (String)e.getKey();
                    
                    
                    VerticeGraph V= (VerticeGraph) Vertices.get(key);
                    
                    if (V.Node1.Id==N1){
                        
                        Node1 = V.Node1;
                        Node1.visited=true;
                       
                    }
                }
                
                
                for (int i=1;i<Node1.adyNodes.size();i++){
      
                    VerticeGraph V1 = Node1.Edges.get(i);
//                    System.out.println(V1.Idv+"  peso "+V1.weight);
                    if(V1.Node1==Node1){
                        V1.Node2.cluster = V1.weight;
                    }else {
                    V1.Node1.cluster = V1.weight;
                }
                    
                    if(!queue.contains(V1)){
                        queue.add(V1);
                    }
               
                }
                System.out.println(queue.size());
                

                while(!queue.isEmpty()){
                    VerticeGraph V1;
                    V1= queue.remove();
                    
                    if(TreeNodes.containsValue(V1.Node1) && TreeNodes.containsValue(V1.Node2)){
                        
                    } else{
                        
                        
                        
                        TreeVertices.put(V1.Idv, V1);
                        
                        NodeGraph n1,n2;
                        n1 = V1.Node1;
                        n2 = V1.Node2;
                        
                       for(int i=1; i<n1.Edges.size();i++ ){
                            VerticeGraph V2;
                            V2= n1.Edges.get(i);
                            if(!queue.contains(V2)){
                                queue.add(V2);
                            }
                        }
                       
                        for(int i=1; i<n2.Edges.size();i++ ){
                            VerticeGraph V2;
                            V2= n2.Edges.get(i);
                            if(!queue.contains(V2)){
                                queue.add(V2);
                            }
                        }
                        
                        TreeNodes.put(V1.Node1.Id, n1);
                        TreeNodes.put(V1.Node2.Id, n2);
                        
                        double Nx= V1.Node1.Id *n;
                        double Ny = V1.Node2.Id*n;
                        String Idv = (V1.Node1.Id+"--"+V1.Node1.Id+";");
                        
                        VerticeGraph.writeFile(Idv, V1.weight ,filename);
                    }
                    
                  
                    System.out.println(V1.Node1.Id+"  peso "+V1.Node1.distance);
                }

                System.out.println(queue.size());
    }

                
                
 ///Todo funciona hasta este punto            
           
    
   
    
    public void Dij(HashMap Nodes, boolean directed, String filename) throws IOException{

        TreeNodes = new HashMap<>();
        TreeVertices = new HashMap<>();

        for(int k=1; k<= Nodes.size(); k++){
            NodeGraph N = (NodeGraph) Nodes.get(k);
            N.visited=false;
        }
        Queue<NodeGraph> treeQueue = new LinkedList<>();
        int node1 = (int)Math.ceil(Math.random() * Nodes.size());
        NodeGraph N1 = (NodeGraph) Nodes.get(node1); 
        
        TreeNodes.put(1, N1);
        treeQueue.add(N1);
        N1.visited = true;
               
        while (!treeQueue.isEmpty()){
             Queue<NodeGraph> nextQueue = new LinkedList<>();
             for(int i=1; i<= treeQueue.size();i++){
              
                 NodeGraph Node1 = treeQueue.peek();
                 treeQueue.poll();
                 for (int j =1; j<= Node1.adyNodes.size();j++){
                     NodeGraph Node2 = Node1.adyNodes.get(j);
                     if(Node2.visited == false){
                         
                         Node2.visited = true;
                         nextQueue.add(Node2);
                         TreeNodes.put(TreeNodes.size()+1, Node2);
                         
                         String Idv = (Node1.Id+"--"+Node2.Id+";");                                      //Create a string for the Vertice Id
                         VerticeGraph newVertice = VerticeGraph.createVertice( Idv, Node1, Node2, false);        // Creates Vertice
                         TreeVertices.put(newVertice.Idv, newVertice);                                                      //put vertice into the HashMaps of Vertices
                         VerticeGraph.writeFile(newVertice.Idv, newVertice.weight ,filename);                                       //Add vertice to the .gv file
                     }
                 }
                 treeQueue = nextQueue;
                 
             }
            
        }
   
    }
    
    
            
      public void Kruskal(HashMap Vertices, boolean directed, String filename) throws IOException{
                
          
          System.out.println(Vertices.size());
                String EdgeKey;
                int clust=0, cluster=1;
                HashMap<Integer,NodeGraph> BFSNodes ;
                HashMap<String,VerticeGraph> BFSVertices;
                     
                TreeNodes = new HashMap<>();
                TreeVertices = new HashMap<>();
                BFSNodes = new HashMap<>();
                BFSVertices = new HashMap<>();

                Queue<VerticeGraph> queue=new PriorityQueue<VerticeGraph>();
          
                Set<HashMap.Entry<String,VerticeGraph>> entrySet = Vertices.entrySet();
                int N1= (int) Math.ceil(Math.random()*Vertices.size()*(0.02));
                NodeGraph Node1 = null;
                for(Entry<String, VerticeGraph> e : entrySet) {
                    String key = e.getKey();
                    
                    
                    VerticeGraph V= (VerticeGraph) Vertices.get(key);
                    queue.add(V);
                    V.Node1.visited=false;
                    V.Node2.visited=false;
                    if (V.Node1.Id==N1){
                        
                        Node1 = V.Node1;
                        
                       
                    }
                }
                         
                
         System.out.println(queue.size());
                
                
//                
                while(!queue.isEmpty()){
                    VerticeGraph V1;
                    V1= queue.remove();
                    
                    TreeVertices.put(V1.Idv, V1);
                    
                    Vertices.remove(V1);
                    
                    if(!TreeNodes.containsValue(V1.Node1)){
                        TreeNodes.put(TreeNodes.size()+1, Node1);
                    }
                    if(!TreeNodes.containsValue(V1.Node2)){
                        TreeNodes.put(TreeNodes.size()+1, V1.Node2);
                    }
                   
                    
                    
     
        Queue<NodeGraph> treeQueue = new LinkedList<>();
        
        NodeGraph Ni = V1.Node1; 
        
        BFSNodes.put(1, Ni);
        treeQueue.add(Ni);
        Ni.visited = true;
               
        while (!treeQueue.isEmpty()){
             Queue<NodeGraph> nextQueue = new LinkedList<>();
             for(int i=1; i<= treeQueue.size();i++){
              
                 NodeGraph Nodei = treeQueue.peek();
                 treeQueue.poll();
                 for (int j =1; j<= Nodei.adyNodes.size();j++){
                     NodeGraph Nodej = Nodei.adyNodes.get(j);
                     
                     
                     if(Nodej!=null && Nodej.visited == false){
                         
                         Nodej.visited = true;
                         nextQueue.add(Nodej);
                         BFSNodes.put(BFSNodes.size()+1, Nodej);
                         
                         String Idv = (Nodei.Id+"--"+Nodej.Id+";");                                      //Create a string for the Vertice Id
                         VerticeGraph newVertice = VerticeGraph.createVertice( Idv, Nodei, Nodej, false);        // Creates Vertice
                         BFSVertices.put(newVertice.Idv, newVertice);                                                      //put vertice into the HashMaps of Vertices
                         
                     }
                 }
                 treeQueue = nextQueue;
                 
             }
             
             System.out.println(TreeNodes.size()+"    Tree");
             System.out.println(BFSNodes.size()+ "      BFS");
             if (TreeNodes.size()==BFSNodes.size()){
                 Vertices.remove(V1);
             }
          
        }
   System.out.println(Vertices.size());
    }
    
                    
      }             
                    
                    
                 
 
  
    

      
      
      
      
      
    public int BFSKrus (HashMap Vertices) throws IOException{

        TreeNodes = new HashMap<>();
        TreeVertices = new HashMap<>();


        Queue<NodeGraph> treeQueue = new LinkedList<>();

        
      
               
        while (!treeQueue.isEmpty()){
             Queue<NodeGraph> nextQueue = new LinkedList<>();
             for(int i=1; i<= treeQueue.size();i++){
              
                 NodeGraph Node1 = treeQueue.peek();
                 treeQueue.poll();
                 for (int j =1; j<= Node1.adyNodes.size();j++){
                     NodeGraph Node2 = Node1.adyNodes.get(j);
                     if(Node2.visited == false){
                         
                         Node2.visited = true;
                         nextQueue.add(Node2);
                         TreeNodes.put(TreeNodes.size()+1, Node2);
                         
                         String Idv = (Node1.Id+"--"+Node2.Id+";");                                      //Create a string for the Vertice Id
                         VerticeGraph newVertice = VerticeGraph.createVertice( Idv, Node1, Node2, false);        // Creates Vertice
                         TreeVertices.put(newVertice.Idv, newVertice);                                                      //put vertice into the HashMaps of Vertices
                         
                     }
                 }
                 treeQueue = nextQueue;
                 
             }
            
        }
   
        return TreeVertices.size();
    }
    
    
     
      
      
}