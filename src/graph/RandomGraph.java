/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graph;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.PriorityQueue;
import javafx.util.Pair;

/**
 *
 * @author Eric Montufar
 */
public class RandomGraph {
 
    
    HashMap<Integer,NodeGraph> E ;                                                      //Nodes Hashmap
    HashMap<String, VerticeGraph> V;                                                    //Vertices HashMap
    HashMap<String,NodeGraph> Ady;
    
    //     GENERATES A RANDOM GRAPH BY USING ERDOS RENYI METHOD
    
    public HashMap genErdosRenyi(int n, int m, boolean directed, boolean autocicle, String filename) throws IOException{
        
        E = new HashMap<>();                                                            // Initialize hashmaps
        V = new HashMap<>();
        
        if (!autocicle){                                                                //Runs when aouticle isnt allowed 
            
              for (int i=1;i<=n;i++){                                                   //Creates Nodes
                  
                NodeGraph node= new NodeGraph(i);                                                           
                E.put(i, node);                                                         //Put the ith Node into the HashMap of nodes
                
          
    }   
        while(V.size()!=m){                                                             //Creates a random int from 1 to n
            
            int node1 = (int)Math.ceil(Math.random()*n);
            int node2 = (int)Math.ceil(Math.random()*n);
  
        
        if (node1 == node2){                                                            // Avoids autocicle
            node2 = (int)Math.ceil(Math.random()*n);
        }
        
        NodeGraph Node1 = E.get(node1);                                                 //Extracts a random node from the HashMap
        NodeGraph Node2 = E.get(node2);                                                 //Extraxts another random node 
                        //check if the vertice already exists

                                                                           // Avoid vertice overwriting
        if (!directed){  
            
            String Idv = (Node1.Id+"--"+Node2.Id);                                      //Create a string for the Vertice Id=
            String Ido = (Node2.Id+"--"+Node1.Id);                                      
            boolean bol =V.containsKey(Idv)|| V.containsKey(Ido);                             // asks if the grah is directed
             if (!bol ){ 
            VerticeGraph newVertice = VerticeGraph.createVertice( Idv, Node1, Node2, false);        // Creates Vertice
//            Idv = (Node1.Id+"--"+Node2.Id+";" + "[label ="   + "\"" + newVertice.weight+"\"");
            V.put(newVertice.Idv, newVertice);                                                       //put vertice into the HashMaps of Vertices
            VerticeGraph.writeFile(newVertice.Idv , newVertice.weight  ,filename);                                       //Add vertice to the .gv file
             }
        } else{
            String Idv = (Node1.Id+"->"+Node2.Id+";"); 
             if ( !V.containsKey(Idv)){ 
            VerticeGraph newVertice = VerticeGraph.createVertice(Idv, Node1, Node2, false);
            V.put(Idv, newVertice);
            VerticeGraph.writeFile(newVertice.Idv, newVertice.weight ,filename);
             }
        }
        

        }
        } else {
           for (int i=1;i<=n;i++){
            NodeGraph node= new NodeGraph(i);
            E.put(i, node);
          
    }   
        while(V.size()!=m){
            
            int node1 = (int)Math.ceil(Math.random()*n);
            int node2 = (int)Math.ceil(Math.random()*n);
 
        
        NodeGraph Node1 = E.get(node1);
        NodeGraph Node2 = E.get(node2);
        
        if (!directed){
            
            String Idv = (Node1.Id+"--"+Node2.Id+";");                                      //Create a string for the Vertice Id
            String Ido = (Node2.Id+"--"+Node1.Id+";");                                      
            boolean bol =V.containsKey(Idv)|| V.containsKey(Ido);                             // asks if the grah is directed
            
            if (!bol){

            VerticeGraph newVertice = VerticeGraph.createVertice( Idv, Node1, Node2, false);        // Creates Vertice
            V.put(newVertice.Idv, newVertice);                                                      //put vertice into the HashMaps of Vertices
            VerticeGraph.writeFile(newVertice.Idv, newVertice.weight ,filename);                                       //Add vertice to the .gv file
             }
            
        } else{
            String Idv = (Node1.Id+"->"+Node2.Id+";"); 
            if (!V.containsKey(Idv)){
            VerticeGraph newVertice = VerticeGraph.createVertice(Idv, Node1, Node2, false);
            V.put(Idv, newVertice);
            VerticeGraph.writeFile(newVertice.Idv, newVertice.weight ,filename);
            }
        }
        
        }    //Contains key
        } 
        
        return V;
        }
        
      
    
    
    
    //  GENERATES A RANDOM GRAPH BY USING GILBERT METHOD
    
    
    public HashMap genGilbert(int n, double p, boolean directed, boolean autocicle, String filename) throws IOException{
       E = new HashMap<>();                                                                             //HashMap of Nodes
       V = new HashMap<>();                                                                             //HashMap of Vertices
        
       if (!autocicle){                                                                                 //autocicle not allowed
        for (int i=1;i<=n;i++){
            NodeGraph node= new NodeGraph(i);                                                           //Creates n nodes
            E.put(i, node);                                                                             //ith node is inserted in the Hashmap
    }
       
        for (int j=1;j<+n;j++){                                                                         //Extracts nodes one by one
          NodeGraph Node1 = E.get(j);
          
          for (int k=1;k<=n;k++){                                                                       
              
              NodeGraph Node2 = E.get(k);                                                               //Extracts the kth node form the hashmap
              double prob = Math.random();                                                              //creates a random number 
              if (prob<=p){                                                                             // chrecks if the random number is less or equal than the probability of crating a new node
              if(j!=k){                                                                                 //autocicles not allowed
                  
                  if (!directed){                                                                       // Not directed graph
                    
                  String Idv = (Node1.Id+"--"+Node2.Id+";");                                            
                  String Ido = (Node2.Id+"--"+Node1.Id+";");  
                  boolean bol = V.containsKey(Idv)|| V.containsKey(Ido);                                // The undirected vertices (N1,N2) and (N2,N1) dont exist. 
                  
                  if(!bol){                                                                             //Avoid Vertices overlapping 
                    
                  VerticeGraph newVertice= VerticeGraph.createVertice(Idv,Node1,Node2, false);          //Crates Vertex
                  V.put(newVertice.Idv,newVertice);                                                     //put new vertex into Vertices Hashmap
                  VerticeGraph.writeFile(newVertice.Idv, newVertice.weight, filename);                                     //Writes the .gv File
                  
              
                  }
                  } else{                                                                               //Directed Graph
                  
                  String Idv = (Node1.Id+"->"+Node2.Id+";");                                            // creates Vertex Id
                  boolean bol = V.containsKey(Idv);                                                     // The directed vertex (N1,N2) doesn't exist

                  if (!bol){                                                                            //Avoids vertices overlapping 
                  
                  VerticeGraph newVertice= VerticeGraph.createVertice(Idv,Node1,Node2, true);           //Creates vertex
                  V.put(newVertice.Idv, newVertice);                                                    //puts vertes into hashmap 
                  VerticeGraph.writeFile(newVertice.Idv, newVertice.weight, filename);                                     //write de .gv file
                    }
              }
              }
              }
          }
        }
       } else {                                                                                         //Autocilces allowed 
            for (int i=1;i<=n;i++){                                                                     // creates n nodes 
            NodeGraph node= new NodeGraph(i);
            E.put(i, node);                                                                             //puts ith nothe into the nodes hashmap 
    }
       
        for (int j=1;j<+n;j++){                                                                         //extracts nodes one by one
          NodeGraph Node1 = E.get(j);
          
          for (int k=1;k<=n;k++){                                               
              
              NodeGraph Node2 = E.get(k);                                                               //gets the kth node from the hashmap
              double prob = Math.random();                                                              //creates a random number 
              
              
              if (prob<=p){                                                                             // chrecks if the random number is less or equal than the probability of crating a new node
                if (!directed){                                                                       // Not directed graph
                    
                  String Idv = (Node1.Id+"--"+Node2.Id+";");                                            
                  String Ido = (Node2.Id+"--"+Node1.Id+";");  
                  boolean bol = V.containsKey(Idv)|| V.containsKey(Ido);                                // The undirected vertices (N1,N2) and (N2,N1) dont exist. 
                  
                  if(!bol){                                                                             //Avoid Vertices overlapping 
                    
                  VerticeGraph newVertice= VerticeGraph.createVertice(Idv,Node1,Node2, false);          //Crates Vertex
                  V.put(newVertice.Idv,newVertice);                                                     //put new vertex into Vertices Hashmap
                  VerticeGraph.writeFile(newVertice.Idv, newVertice.weight, filename);                                     //Writes the .gv File
                  
                 
                  }
                  } else{                                                                               //Directed Graph
                  
                  String Idv = (Node1.Id+"->"+Node2.Id+";");                                            // creates Vertex Id
                  boolean bol = V.containsKey(Idv);                                                     // The directed vertex (N1,N2) doesn't exist

                  if (!bol){                                                                            //Avoids vertices overlapping 
                  
                  VerticeGraph newVertice= VerticeGraph.createVertice(Idv,Node1,Node2, true);           //Creates vertex
                  V.put(newVertice.Idv, newVertice);                                                    //puts vertes into hashmap 
                  VerticeGraph.writeFile(newVertice.Idv, newVertice.weight, filename);                                     //write de .gv file
                    }
              }
          }
        }
           
       }
    }

       return V;
   
    }
    
    
    // CREATES A NEW RANDOM GRAPH BY USING THE BARABASI-ALBERT METHOD 
     
    public HashMap GenBarabasiAlbert(int n, double g, boolean directed, boolean autocicle, String filename) throws IOException{
       E = new HashMap<>();                                                                             //HashMap of Nodes
       V = new HashMap<>();                                                                             //HashMap of Vertices
        
       if (!autocicle){                                                                                 //autocicle not allowed
        for (int i=1;i<=n;i++){
            NodeGraph Node1= new NodeGraph(i);                                                           //Creates n nodes
            E.put(i, Node1);                                                                             //ith node is inserted in the Hashmap
            
            for(int j=1; j<i;j++){                                                                      //No Autocicles allowed 
            
                NodeGraph Node2 = E.get(j);                                                             //Extracts nodes one by one from the hashtable 
                double p = 1 - (Node2.grade/g);                                                         //probability of creating a new node based on the node's grade
                double prob = Math.random();                                                            //Creates a random number 

                if (p > prob && Node1.grade <= g && Node2.grade <= g){                                  // probability and ranges of both nodes in accepted value 
                        
                  if (!directed){                                                                       // Not directed graph
                    
                  String Idv = (Node1.Id+"--"+Node2.Id+";");                                            
                  String Ido = (Node2.Id+"--"+Node1.Id+";");  
                  boolean bol = V.containsKey(Idv)|| V.containsKey(Ido);                                // The undirected vertices (N1,N2) and (N2,N1) dont exist. 
                  
                  if(!bol){                                                                             //Avoid Vertices overlapping 
                    
                  VerticeGraph newVertice= VerticeGraph.createVertice(Idv,Node1,Node2, false);          //Crates Vertex
                  V.put(newVertice.Idv,newVertice);                                                     //put new vertex into Vertices Hashmap
                  VerticeGraph.writeFile(newVertice.Idv, newVertice.weight, filename);                                     //Writes the .gv File
                  
                  }
                  } else{                                                                               //Directed Graph
                  
                  String Idv = (Node1.Id+"->"+Node2.Id+";");                                            // creates Vertex Id
                  boolean bol = V.containsKey(Idv);                                                     // The directed vertex (N1,N2) doesn't exist

                  if (!bol){                                                                            //Avoids vertices overlapping 
                  
                  VerticeGraph newVertice= VerticeGraph.createVertice(Idv,Node1,Node2, true);           //Creates vertex
                  V.put(newVertice.Idv, newVertice);                                                    //puts vertes into hashmap 
                  VerticeGraph.writeFile(newVertice.Idv, newVertice.weight, filename);                                     //write de .gv file
                    }
                    }
                    }
                }
    }

       } else {  
           for (int i=1;i<=n;i++){
            NodeGraph Node1= new NodeGraph(i);                                                           //Creates n nodes
            E.put(i, Node1);                                                                             //ith node is inserted in the Hashmap
            
            for(int j=1; j<=i;j++){                                                                     //Allows autocicles 
                
                    NodeGraph Node2 = E.get(j);                                                         //Extracts nodes one by one from the hashtable
                    double p = 1 - (Node2.grade/g);                                                     //probability of creating a new node based on the node's grade
                    double prob = Math.random();                                                        //Creates a random number
                    
                    if (p > prob && Node1.grade <= g && Node2.grade <= g){                              // probability and ranges of both nodes in accepted value 
                        
                  if (!directed){                                                                       // Not directed graph
                    
                  String Idv = (Node1.Id+"--"+Node2.Id+";");                                            
                  String Ido = (Node2.Id+"--"+Node1.Id+";");  
                  boolean bol = V.containsKey(Idv)|| V.containsKey(Ido);                                // The undirected vertices (N1,N2) and (N2,N1) dont exist. 
                  
                  if(!bol){                                                                             //Avoid Vertices overlapping 
                    
                  VerticeGraph newVertice= VerticeGraph.createVertice(Idv,Node1,Node2, false);          //Crates Vertex
                  V.put(newVertice.Idv,newVertice);                                                     //put new vertex into Vertices Hashmap
                  VerticeGraph.writeFile(newVertice.Idv, newVertice.weight, filename);                                     //Writes the .gv File
                  
                  }
                  } else{                                                                               //Directed Graph
                  
                  String Idv = (Node1.Id+"->"+Node2.Id+";");                                            // creates Vertex Id
                  boolean bol = V.containsKey(Idv);                                                     // The directed vertex (N1,N2) doesn't exist

                  if (!bol){                                                                            //Avoids vertices overlapping 
                  
                  VerticeGraph newVertice= VerticeGraph.createVertice(Idv,Node1,Node2, true);           //Creates vertex
                  V.put(newVertice.Idv, newVertice);                                                    //puts vertes into hashmap 
                  VerticeGraph.writeFile(newVertice.Idv, newVertice.weight, filename);                                     //write de .gv file
                    }
                    }
                    }
                }
    }
    }
       return V;
    }
    
    
    
    
        //     GENERATES A RANDOM GRAPH BY USING ERDOS RENYI METHOD
    
public HashMap genGeographic(int n, double r, boolean directed, boolean autocicle, String filename) throws IOException{
       E = new HashMap<>();                                                                             //HashMap of Nodes
       V = new HashMap<>();                                                                             //HashMap of Vertices
       double X1,Y1,X2,Y2;
       
       if (!autocicle){                                                                                 //autocicle not allowed
        for (int i=1;i<=n;i++){
            NodeGraph node= new NodeGraph(i);                                                           //Creates n nodes
            node.x = Math.random();
            node.y = Math.random();
            E.put(i, node);                                                                             //ith node is inserted in the Hashmap
        }
       
        for (int j=1;j<+n;j++){                                                                         //Extracts nodes one by one
          NodeGraph Node1 = E.get(j);
          
          for (int k=1;k<=n;k++){                                                                       
              
              NodeGraph Node2 = E.get(k);                                                               //Extracts the kth node form the hashmap
                  
               X1 = Node1.x;
               Y1 = Node1.y;
               
               X2 = Node2.x;
               Y2 = Node2.y;
               
               double dx = Math.abs(X1-X2);
               double dy = Math.abs(Y1-Y2);
               double dist = Math.sqrt((dx*dx)+(dy*dy));
               
               if (dist<=r && j!=k){                                                                           // chrecks if the random number is less or equal than the probability of crating a new node
               if(j!=k){
                  
                  if (!directed){                                                                       // Not directed graph
                    
                  String Idv = (Node1.Id+"--"+Node2.Id+";");                                            
                  String Ido = (Node2.Id+"--"+Node1.Id+";");  
                  boolean bol = V.containsKey(Idv)|| V.containsKey(Ido);                                // The undirected vertices (N1,N2) and (N2,N1) dont exist. 
                  
                  if(!bol){                                                                             //Avoid Vertices overlapping 
                    
                  VerticeGraph newVertice= VerticeGraph.createVertice(Idv,Node1,Node2, false);          //Crates Vertex
                  V.put(newVertice.Idv,newVertice);                                                     //put new vertex into Vertices Hashmap
                  VerticeGraph.writeFile(newVertice.Idv, newVertice.weight, filename);                                     //Writes the .gv File
                  
                  }
                  } else{                                                                               //Directed Graph
                  
                  String Idv = (Node1.Id+"->"+Node2.Id+";");                                            // creates Vertex Id
                  boolean bol = V.containsKey(Idv);                                                     // The directed vertex (N1,N2) doesn't exist

                  if (!bol){                                                                            //Avoids vertices overlapping 
                  
                  VerticeGraph newVertice= VerticeGraph.createVertice(Idv,Node1,Node2, true);           //Creates vertex
                  V.put(newVertice.Idv, newVertice);                                                    //puts vertes into hashmap 
                  VerticeGraph.writeFile(newVertice.Idv, newVertice.weight, filename);                                     //write de .gv file
                    }
              }
              }
              }
          }
        }
       } else {                                                                                         //Autocilces allowed 
            for (int i=1;i<=n;i++){
            NodeGraph node= new NodeGraph(i);                                                           //Creates n nodes
            node.x = Math.random();
            node.y = Math.random();
            E.put(i, node);                                                                             //ith node is inserted in the Hashmap
        }
       
        for (int j=1;j<+n;j++){                                                                         //Extracts nodes one by one
          NodeGraph Node1 = E.get(j);
          
          for (int k=1;k<=n;k++){                                                                       
              
              NodeGraph Node2 = E.get(k);                                                               //Extracts the kth node form the hashmap
                  
               X1 = Node1.x;
               Y1 = Node1.y;
               
               X2 = Node2.x;
               Y2 = Node2.y;
               
               double dx = Math.abs(X1-X2);
               double dy = Math.abs(Y1-Y2);
               double dist = Math.sqrt((dx*dx)+(dy*dy));
               
               if (dist<=r && j!=k){                                                                           // chrecks if the random number is less or equal than the probability of crating a new node
          
                  if (!directed){                                                                       // Not directed graph
                    
                  String Idv = (Node1.Id+"--"+Node2.Id+";");                                            
                  String Ido = (Node2.Id+"--"+Node1.Id+";");  
                  boolean bol = V.containsKey(Idv)|| V.containsKey(Ido);                                // The undirected vertices (N1,N2) and (N2,N1) dont exist. 
                  
                  if(!bol){                                                                             //Avoid Vertices overlapping 
                    
                  VerticeGraph newVertice= VerticeGraph.createVertice(Idv,Node1,Node2, false);          //Crates Vertex
                  V.put(newVertice.Idv,newVertice);                                                     //put new vertex into Vertices Hashmap
                  VerticeGraph.writeFile(newVertice.Idv, newVertice.weight, filename);                                     //Writes the .gv File
                  
                  }
                  } else{                                                                               //Directed Graph
                  
                  String Idv = (Node1.Id+"->"+Node2.Id+";");                                            // creates Vertex Id
                  boolean bol = V.containsKey(Idv);                                                     // The directed vertex (N1,N2) doesn't exist

                  if (!bol){                                                                            //Avoids vertices overlapping 
                  
                  VerticeGraph newVertice= VerticeGraph.createVertice(Idv,Node1,Node2, true);           //Creates vertex
                  V.put(newVertice.Idv, newVertice);                                                    //puts vertes into hashmap 
                  VerticeGraph.writeFile(newVertice.Idv, newVertice.weight, filename);                                     //write de .gv file
                    }
              }
              
              }
          }
        }
    }
       return V;
} 
}