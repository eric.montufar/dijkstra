/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graph;


import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Eric Montufar
 */
public class VerticeGraph implements Comparable<VerticeGraph> {
    
    NodeGraph Node1, Node2;
    String Idv;
    int cluster;
    int  weight;
    
   public static VerticeGraph newVertice = null;
   
   private VerticeGraph (){   
   }
    
   public static VerticeGraph createVertice(){
            if (newVertice == null){
                newVertice = new VerticeGraph();                        //Singleton
            }
            return newVertice;
   }
   
   public VerticeGraph(String Idv, int n1, int n2){
  
   }
   
   
     public static  VerticeGraph createVertice(String Idv, NodeGraph N1, NodeGraph N2, boolean directed){
            VerticeGraph newVertice = new VerticeGraph(Idv ,N1.Id,N2.Id );
            newVertice.Idv = Idv;
            newVertice.Node1=N1;
            newVertice.Node2=N2;
           int peso = (int) Math.ceil(Math.random()*20);
            newVertice.weight=(peso);
            newVertice.cluster =0;
            
            N1.grade = N1.grade +1;
            N2.grade = N2.grade +1;

            N1.Edges.put(N1.Edges.size()+1, newVertice);
            N2.Edges.put(N2.Edges.size()+1, newVertice);
            
            N1.adyNodes.put(N1.grade, N2);
            N2.adyNodes.put(N2.grade, N1);
            
           return newVertice;
       }
     
     
        public static void writeFile(String vertice, double weight,String filename) throws IOException{
         File file;
         FileWriter write;
         PrintWriter line;
         
         
         file= new File("D:\\Desktop\\"+filename+".gv");
         
         if (!file.exists()){
             try {
                 file.createNewFile();
                 write =  new FileWriter(file, true);
                 line= new PrintWriter(write);
                 
                 line.println("graph "+filename+" {");
                 line.print(vertice);
                 line.print(" [ Label = ");
                 line.print('"');
                 line.print(weight);
                 line.print('"');
                 line.print("]");
                 line.print("; ");
                 line.println("");
                 
                 line.close();
                 write.close();
             } catch (IOException ex) {
                 Logger.getLogger(VerticeGraph.class.getName()).log(Level.SEVERE, null, ex);
             }
             
         } else {
             try {
                 write =  new FileWriter(file, true);
                 line= new PrintWriter(write);
                 
                 line.print(vertice);
                 line.print(" [ Label = ");
                 line.print('"');
                 line.print(weight);
                 line.print('"');
                 line.print("]");
                 line.print("; ");
                 line.println("");
                 //System.out.println(vertice);
                 line.close();
                 write.close();
             } catch (IOException ex) {
                 Logger.getLogger(VerticeGraph.class.getName()).log(Level.SEVERE, null, ex);
             }
         }
//         write =  new FileWriter(file, true);
//         line= new PrintWriter(write);
//         line.println("}");
    }
     
     
        public int compareTo(VerticeGraph v) {  
        if(weight>=v.weight){  
            return 1;  
        }else if(weight<v.weight){  
            return -1;  
        }else{  
            return 0;  
    }  
}  
     
    
        
        
        
        
        
        
        
        
        
        
     
}
